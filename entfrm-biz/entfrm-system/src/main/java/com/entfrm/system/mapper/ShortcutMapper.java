package com.entfrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.system.entity.Shortcut;

/**
 * @author entfrm
 * @date 2019-08-25 22:56:58
 *
 * @description 快捷方式Mapper接口
 */
public interface ShortcutMapper extends BaseMapper<Shortcut>{

}
