package com.entfrm.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.system.entity.Dict;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author entfrm
 * @since 2019-01-30
 */
public interface DictMapper extends BaseMapper<Dict> {

}
